from flask import Flask, render_template
app = Flask(__name__)


context = [
    {
        'title': 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Dolorem, sequi?',
        'content':'Lorem ipsum dolor sit amet consectetur adipisicing elit. Numquam eius odio fuga eum in deleniti sapiente perferendis minus amet.<br>Lorem ipsum dolor sit amet consectetur adipisicing elit. Expedita esse atque labore libero reprehenderit?illum et sapiente esse eum magni debitis consequatur quaerat alias? Cumque facilis dicta eum. Numquam eius odio fuga eum in deleniti sapiente perferendis minus amet Illum ea nesciunt est repellat explicabo facilis ad facere ab! Error illum et sapiente esse eum magni debitis consequatur quaerat alias? Cumque facilis dicta eum. Numquam eius odio fuga eum in deleniti sapiente perferendis minus amet Illum ea nesciunt est repellat explicabo facilis ad facere ab! Error illum et sapiente esse eum magni debitis consequatur quaerat alias? Cumque facilis dicta eum. Numquam eius odio fuga eum in deleniti sapiente perferendis minus amet.',
        'picture': [
            {'image': './static/pictures/2f61529b72bf422=s1900.png'},
            {'image': './static/pictures/23.png'},
            {'image': './static/pictures/1900.png'}
        ],
        'icons': [
            {'icon': './static/icons/1972.svg', 'textIcon': 'pools'},
            {'icon': './static/icons/1974.svg', 'textIcon': 'splash water park'},
            {'icon': './static/icons/1976.svg', 'textIcon': 'solarium & relax zones'},
            {'icon': './static/icons/1977.svg', 'textIcon': 'water sports'},
            ]
    }
]

@app.route("/")
def template_test():
    return render_template('index.html', cardHotel=context)



if __name__ == '__main__':
    app.run(debug=True)
