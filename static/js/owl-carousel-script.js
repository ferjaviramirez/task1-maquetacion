$(document).ready(function(){
    $(".owl-carousel").owlCarousel({
      items:1,
      autoplay:true,
      singleItem:true,
      loop:true,
      nav:true,
      navText : ["<i class='fa fa-arrow-left'></i>","<i class='fa fa-arrow-right'></i>"]
    });
  });